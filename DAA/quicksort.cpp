#include<iostream>
#include<time.h>
#define N 20

using namespace std;
void reduction(int a[],int beg, int end,int *loc)
{
	int left,right,temp;
	left=*loc=beg;
	right=end;
	int done=0;
	while(!done)
	{
		while((a[*loc]<a[right])&&(*loc!=right))
			right--;
		if(*loc==right)
			done=1;
		else
		{
			temp=a[*loc];
			a[*loc]=a[right];
			a[right]=temp;
			*loc=right;
		}
		if(!done)
		{
			while((a[*loc]>a[left])&&(*loc!=left))
				left++;
			if(*loc==left)
				done=1;
			else
			{
				temp=a[*loc];
				a[*loc]=a[left];
				a[left]=temp;
				*loc=left;
			}
		}
	}
}

void quickr(int a[],int lb,int ub)
{
	int loc;
	if(lb<ub)
	{
		reduction(a,lb,ub,&loc);
		quickr(a,lb,loc-1);
		quickr(a,loc+1,ub);
	}
}


int main()
{
	int q[N],n;
	cout<<"roll no is 1311017"<<endl;
	cout<<"\nNumber of elements in array: ";
	cin>>n;
	cout<<"\nEnter elements in array: ";
	for(int i=0;i<n;i++)
	cin>>q[i];
	quickr(q,0,n-1);
	cout<<"\nsorted array is:\n";
	for(int i=0;i<n;i++)
	cout<<q[i]<<endl;
}
