#include<iostream>
using namespace std;

int binary_search( int* arry , int no ,int upper , int lower ){
	int middle;
    middle=(upper+lower)/2;

    if(arry[middle] == no)
        return middle;
    if(upper==lower)
        return -1;
    else{
        if(arry[middle]<no)
            lower=middle+1;
        if(arry[middle]>no)
            upper=middle-1;
        binary_search(arry,no,upper,lower);
    }
}

int insertion_sort(int* arry , int n){
    for( int i=1;i<n ;i++){
            for(int j = 0 ; j<i;j++){
                if(arry[i-j]<arry[i-j-1])
                    swap(arry[i-j],arry[i-j-1]);
                else
                    break;
            }
    }
}


int main()
{
	int a[10] = { 0,3,4,5,56,34,10,11,31,1};
	int b = 10;
	insertion_sort(a,b);
    for( int i = 0 ; i < b ; i++ )
        cout<<a[i]<<"  ";
}
