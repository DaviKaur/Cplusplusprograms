#include<iostream>
using namespace std ;

/*swap two no. */
void swapint(int*a , int n ,int m){
    int temp = a[n];
    a[n] = a[m];
    a[m] = temp;
}

/*correct heap*/
void max_heapify(int *a , int i,int n ){
    int j=2*i;
    if(j<n){
        if( a[j] > a[j+1] ){
        	
            swap(a[i],a[j]);
        }
        else{
            swap(a[i],a[j+1]);
            j=j+1;
        }
        max_heapify(a,j,n);
    }

}
/*build max heap */
void max_heap(int *a , int n){
    for (int i = n/2 ;i>=1;i--){
            if( a[i] >= a[2*i] && a[i] >= a[2*i+1] )
                continue;
            else
                max_heapify(a,i,n);
    }
}
/* sort array using heap sort */
void heap_sort(int *arr , int n){
    for( int i = n ; i >= 1 ; i-- ){
        max_heap(arr,i);
        swap(arr[i],arr[1]);
    }
       if(arr[1]>arr[2])
       	swap(arr[1],arr[2]);
       		
  
}


int main (){
    clock_t start ,end;
	start =clock();
    int b;
	cout<<"enter no of elements";
	cin>>b;
	int arry[b+1];
	arry[0]=0;
	for (int i=1 ;i<=b ;i++ ){
		cout<<"enter element";
		cin>>arry[i];
	}
    heap_sort(arry,b);
     for( int i = 1 ; i <= b ; i++ ){
        cout<<arry[i]<<"  ";
    }
    end=clock();
	cout<<endl;
	cout<<"time taken is "<<float(end-start)/CLOCKS_PER_SEC<<"sec"<<endl;
}
