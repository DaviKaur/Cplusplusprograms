#include<iostream>
#include<stdlib.h>
#include<string.h>
#define STRING_X "Tue"
#define STRING_Y "WED"

using namespace std;

#define SENTINEL (-1)
#define EDIT_COST (1)

inline
  int min(int a, int b)
  {
  return a < b ? a : b;
  }
  int Minimum(int a, int b, int c)
  {
    return min(min(a, b), c);
  }
int EditDistanceDP(char X[], char Y[])
{
  int cost = 0;
  int leftCell, topCell, cornerCell;
  int m = strlen(X)+1;
  int n = strlen(Y)+1;
  int T[m][n];
  for(int i = 0; i < m; i++)
    for(int j = 0; j < n; j++)
      T[i][j] = SENTINEL;
  for(int i = 0; i < m; i++)
    T[i][0] = i;
  for(int j = 0; j < n; j++)
    T[0][j] = j;
  for(int i = 1; i < m; i++)
  {
    for(int j = 1; j < n; j++)
    {
      leftCell = T[i][j-1];
      leftCell += EDIT_COST;
      topCell = T[i-1][j];
      topCell += EDIT_COST;
      cornerCell = T[i-1][j-1];
      cornerCell += (X[i-1] != Y[j-1]);
      T[i][j] = Minimum(leftCell, topCell, cornerCell);
    }
  }
  cost = T[m-1][n-1];
 		for(int i=0;i<m;i++){
 			cout<<endl;
 		 for(int j=0;j<n;j++)
			cout<<T[i][j]<<" ";
		}
  return cost;
}
int main()
{
  char X[] = STRING_X;
  char Y[] = STRING_Y;
  cout<<"Roll no is 1311017"<<endl;
  cout<<"\nMinimum edits required to convert "<<X<<" into "<<Y<<" is\n"<<EditDistanceDP(X, Y)<<endl;
}
